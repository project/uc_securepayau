uc_securepayau
~~~~~~~~~~~~~~

Integration between ubercart and securepay.com.au payment gateway

INSTALL
~~~~~~~

See the getting started guild on installing drupal modules:
http://drupal.org/getting-started/install-contrib/modules

Visit admin/store/settings/payment and enable the gateway, click on
'settings' and then the tab 'SecurePay Gateway'


CREDITS
~~~~~~~
Chris Hood - (http://univate.com.au)
Leigh Morresi - (https://dgtlmoon.com)

LICENSE
~~~~~~~
No guarantee is provided with this software, no matter how critical your
information, module authors are not responsible for damage caused by this
software or obligated in any way to correct problems you may experience.

This software licensed under the GNU General Public License 2.0.
http://www.gnu.org/licenses/gpl-2.0.txt
